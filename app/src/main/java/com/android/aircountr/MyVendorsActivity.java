package com.android.aircountr;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.aircountr.fragment.VendorsListFragment;
import com.android.aircountr.objects.CategoryListItem;

import java.util.ArrayList;
import java.util.List;

import io.karim.MaterialTabs;


/**
 * Created by gaurav on 4/29/2016.
 */
public class MyVendorsActivity extends FragmentActivity implements View.OnClickListener {
    private String TAG = this.getClass().getSimpleName();
    private ImageView iv_backBtn;
    private TextView tv_pageTitle;
    private MaterialTabs tabs;
    private ViewPager viewPager;
    public MaterialDialog processing;
    public Typeface REGULAR, SEMIBOLD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_vendors);

        REGULAR = Typeface.createFromAsset(getAssets(), "ProximaNova-Regular.ttf");
        SEMIBOLD = Typeface.createFromAsset(getAssets(), "ProximaNova-Semibold.ttf");

        iv_backBtn = (ImageView) findViewById(R.id.iv_backBtn);
        tv_pageTitle = (TextView) findViewById(R.id.tv_pageTitle);
        tv_pageTitle.setTypeface(SEMIBOLD);
        tabs = (MaterialTabs) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        setupViewPager(viewPager, ((AircountrApplication) getApplicationContext()).mCategoryListItems);

        iv_backBtn.setOnClickListener(this);
    }

    private void setupViewPager(ViewPager viewPager, ArrayList<CategoryListItem> mDataList) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(), mDataList);
        viewPager.setAdapter(adapter);
        tabs.setViewPager(viewPager);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private List<CategoryListItem> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager, List<CategoryListItem> mFragmentTitleList) {
            super(manager);
            this.mFragmentTitleList = mFragmentTitleList;
        }

        @Override
        public Fragment getItem(int position) {
            VendorsListFragment mVendorsListFragment = new VendorsListFragment();
            mVendorsListFragment.mVendorsList = this.mFragmentTitleList.get(position).getVendorsDataList();
            mVendorsListFragment.categoryId = this.mFragmentTitleList.get(position).getCategoryId();
            mVendorsListFragment.categoryName = this.mFragmentTitleList.get(position).getCategoryName();
            return mVendorsListFragment;
        }

        @Override
        public int getCount() {
            return mFragmentTitleList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position).getCategoryName().toUpperCase();
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.iv_backBtn:
                MyVendorsActivity.this.finish();
                break;
        }
    }
}