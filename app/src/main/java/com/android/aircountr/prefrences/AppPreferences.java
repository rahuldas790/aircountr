package com.android.aircountr.prefrences;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * Created by gaurav on 5/1/2016.
 */
public class AppPreferences {
    private static SharedPreferences pref;
    private static Editor editor;
    private Context _context;
    private int PRIVATE_MODE = 0;
    private static final String PREFER_NAME = "AircountrPrefs";
    public static String KEY_ACCESS_TOKEN = "token";
    public static String KEY_MERCHANT_ID = "merchantId";
    public static String KEY_BUSINESS_NAME = "businessname";
    public static String KEY_EMAIL_ID = "email";
    public static String KEY_PHONE_NO = "mobile";
    public static String KEY_OPERATIONAL_HOUR_FROM = "openTime";
    public static String KEY_OPERATIONAL_HOUR_TO = "closeTime";

    public AppPreferences(Context context) {
        this._context = context;
        this.pref = this._context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
        editor.commit();
    }

    public static String getAccessToken(Context _context) {
        SharedPreferences sharedPreferences = _context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_ACCESS_TOKEN, "");
    }

    public static void setAccessToken(Context _context, String accessToken) {
        Editor editor = _context.getSharedPreferences(PREFER_NAME, _context.MODE_PRIVATE).edit();
        editor.putString(KEY_ACCESS_TOKEN, accessToken);
        editor.commit();
    }

    public static String getMerchantId(Context _context) {
        SharedPreferences sharedPreferences = _context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_MERCHANT_ID, "");
    }

    public static void setMerchantId(Context _context, String merchantId) {
        Editor editor = _context.getSharedPreferences(PREFER_NAME, _context.MODE_PRIVATE).edit();
        editor.putString(KEY_MERCHANT_ID, merchantId);
        editor.commit();
    }
    public static String getBusinessName(Context _context) {
        SharedPreferences sharedPreferences = _context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_BUSINESS_NAME, "");
    }

    public static void setBusinessName(Context _context, String businessName) {
        Editor editor = _context.getSharedPreferences(PREFER_NAME, _context.MODE_PRIVATE).edit();
        editor.putString(KEY_BUSINESS_NAME, businessName);
        editor.commit();
    }
    public static String getEmailId(Context _context) {
        SharedPreferences sharedPreferences = _context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_EMAIL_ID, "");
    }

    public static void setEmailId(Context _context, String emailId) {
        Editor editor = _context.getSharedPreferences(PREFER_NAME, _context.MODE_PRIVATE).edit();
        editor.putString(KEY_EMAIL_ID, emailId);
        editor.commit();
    }
    public static String getPhoneNo(Context _context) {
        SharedPreferences sharedPreferences = _context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_PHONE_NO, "");
    }

    public static void setPhoneNo(Context _context, String mobile) {
        Editor editor = _context.getSharedPreferences(PREFER_NAME, _context.MODE_PRIVATE).edit();
        editor.putString(KEY_PHONE_NO, mobile);
        editor.commit();
    }
    public static String getOperationalHourFrom(Context _context) {
        SharedPreferences sharedPreferences = _context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_OPERATIONAL_HOUR_FROM, "");
    }

    public static void setOperationalHourFrom(Context _context, String op_hourfrom) {
        Editor editor = _context.getSharedPreferences(PREFER_NAME, _context.MODE_PRIVATE).edit();
        editor.putString(KEY_OPERATIONAL_HOUR_FROM, op_hourfrom);
        editor.commit();
    }
    public static String getOperationalHourTo(Context _context) {
        SharedPreferences sharedPreferences = _context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_OPERATIONAL_HOUR_TO, "");
    }

    public static void setOperationalHourTo(Context _context, String op_hourTo) {
        Editor editor = _context.getSharedPreferences(PREFER_NAME, _context.MODE_PRIVATE).edit();
        editor.putString(KEY_OPERATIONAL_HOUR_TO, op_hourTo);
        editor.commit();
    }

}