package com.android.aircountr.smoochIo;

import android.app.Application;

import com.android.aircountr.AircountrApplication;

import io.smooch.core.Smooch;

/**
 * Created by Saahil on 11/06/16.
 */
public class ApplicationSmooch extends AircountrApplication {
    @Override
    public void onCreate() {
        super.onCreate();

        Smooch.init(this, "9stpiod5dket0hx6l9h1363n7");
    }
}
