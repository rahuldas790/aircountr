package com.android.aircountr;

import android.app.Application;
import android.text.TextUtils;

import com.android.aircountr.objects.CategoryListItem;
import com.android.aircountr.utils.LruBitmapCache;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;

import io.smooch.core.Smooch;

/**
 * Created by architnf on 5/17/2016.
 */
public class AircountrApplication extends Application {
    public static final String TAG = AircountrApplication.class
            .getSimpleName();
    public ArrayList<CategoryListItem> mCategoryListItems;
    public String mCurrentPhotoPath;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private static AircountrApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mCategoryListItems = new ArrayList<>();
        mInstance = this;
        Smooch.init(this, "9stpiod5dket0hx6l9h1363n7");
    }

    public static synchronized AircountrApplication getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}