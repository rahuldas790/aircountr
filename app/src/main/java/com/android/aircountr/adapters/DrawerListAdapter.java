package com.android.aircountr.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.aircountr.R;

import java.util.ArrayList;

/**
 * Created by architnf on 5/16/2016.
 */
public class DrawerListAdapter extends BaseAdapter {

    private String TAG = this.getClass().getSimpleName();
    private ArrayList<String> mDataList;
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    public Typeface REGULAR;

    public DrawerListAdapter(Context mContext, ArrayList<String> mDataList) {
        this.mContext = mContext;
        this.mDataList = mDataList;
        this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        REGULAR = Typeface.createFromAsset(mContext.getAssets(), "ProximaNova-Regular.ttf");
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mLayoutInflater.inflate(R.layout.row_drawer_list, null);
        ImageView itemIcon = (ImageView) convertView.findViewById(R.id.iv_itemIcon);
        TextView itemName = (TextView) convertView.findViewById(R.id.tv_itemName);
        itemName.setText((String) getItem(position));
        itemName.setTypeface(REGULAR);
        switch ((String) getItem(position)) {
            case "My Invoices":
                itemIcon.setImageResource(R.drawable.icon_invoice);
                break;
            case "My Vendors":
                itemIcon.setImageResource(R.drawable.icon_vendors_list);
                break;
            case "Reports":
                itemIcon.setImageResource(R.drawable.icon_reports);
                break;
            case "Rating":
                itemIcon.setImageResource(R.drawable.icon_rating);
                break;
            case "Share":
                itemIcon.setImageResource(R.drawable.icon_share);
                break;
            case "Settings":
                itemIcon.setImageResource(R.drawable.icon_settings);
                break;
            case "Contact Us":
                itemIcon.setImageResource(R.drawable.icon_online_support);
                break;
        }
        return convertView;
    }
}
