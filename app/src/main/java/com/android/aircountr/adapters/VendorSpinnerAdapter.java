package com.android.aircountr.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.aircountr.R;
import com.android.aircountr.objects.CategoryListItem;

import java.util.ArrayList;

/**
 * Created by gaura on 5/19/2016.
 */
public class VendorSpinnerAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<CategoryListItem.Vendors> mDataList;
    private LayoutInflater mLayoutInflater;
    public Typeface REGULAR;

    public VendorSpinnerAdapter(Context mContext, ArrayList<CategoryListItem.Vendors> mDataList) {
        this.mContext = mContext;
        this.mDataList = mDataList;
        this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        REGULAR = Typeface.createFromAsset(mContext.getAssets(), "ProximaNova-Regular.ttf");
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

//    public void onDataSetChanged(ArrayList<CategoryListItem.Vendors> mDataList) {
//        this.mDataList = mDataList;
//        notifyDataSetChanged();
//    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
//        ViewHolder viewHolder = null;
//        if (convertView == null) {
//            viewHolder = new ViewHolder();
            convertView = mLayoutInflater.inflate(R.layout.row_vendor_spinner, null);

            TextView tv_vendorName = (TextView) convertView.findViewById(R.id.tv_vendorName);
            tv_vendorName.setTypeface(REGULAR);

//            convertView.setTag(viewHolder);
//        } else {
//            viewHolder = (ViewHolder) convertView.getTag();
//        }

       tv_vendorName.setText(mDataList.get(position).getVendorName());

        return convertView;
    }

//    private class ViewHolder {
//        private TextView tv_vendorName;
//    }
}
