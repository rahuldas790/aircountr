package com.android.aircountr.objects;

/**
 * Created by gaurav on 5/2/2016.
 */
public class GPSAddressListItems {
    String areaName;
    String address;
    String placeId;

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }
}
