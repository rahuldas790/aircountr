package com.android.aircountr.objects;

import java.util.ArrayList;

/**
 * Created by gaurav on 5/5/2016.
 */
public class CategoryListItem {
    String categoryId;
    String resourceId;
    String merchantId;
    String categoryName;
    boolean isVisible;
    ArrayList<Vendors> vendorsDataList;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setIsVisible(boolean isVisible) {
        this.isVisible = isVisible;
    }

    public ArrayList<Vendors> getVendorsDataList() {
        return vendorsDataList;
    }

    public void setVendorsDataList(ArrayList<Vendors> vendorsDataList) {
        this.vendorsDataList = vendorsDataList;
    }

    public class Vendors {
        String categoryId;
        String category;
        String merchantId;
        String vendorAddress;
        String vendorNumber;
        String vendorName;
        String vendorId;

        public String getVendorId() {
            return vendorId;
        }

        public void setVendorId(String vendorId) {
            this.vendorId = vendorId;
        }

        public String getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(String categoryId) {
            this.categoryId = categoryId;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getMerchantId() {
            return merchantId;
        }

        public void setMerchantId(String merchantId) {
            this.merchantId = merchantId;
        }

        public String getVendorAddress() {
            return vendorAddress;
        }

        public void setVendorAddress(String vendorAddress) {
            this.vendorAddress = vendorAddress;
        }

        public String getVendorNumber() {
            return vendorNumber;
        }

        public void setVendorNumber(String vendorNumber) {
            this.vendorNumber = vendorNumber;
        }

        public String getVendorName() {
            return vendorName;
        }

        public void setVendorName(String vendorName) {
            this.vendorName = vendorName;
        }
    }
}