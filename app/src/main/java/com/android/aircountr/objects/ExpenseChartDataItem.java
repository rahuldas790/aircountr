package com.android.aircountr.objects;

/**
 * Created by gaura on 5/21/2016.
 */
public class ExpenseChartDataItem {
    String name;
    String amount;
    int percentage;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }
}
